//code generate automatic not edit date: 2023-11-24T01:42:29.611Z
import Worker from "onbbu/worker";

import * as Services from "../services";
import * as Validate from "../validate";
import * as Middlewares from "../middlewares";

import { Endpoint, name } from "../types";

const worker: Worker<Endpoint> = new Worker<Endpoint>(name);

worker.use("create", Validate.create, Middlewares.create, Services.create);

worker.use("update", Validate.update, Middlewares.update, Services.update);

worker.use("destroy", Validate.destroy, Middlewares.destroy, Services.destroy);

worker.use("findAndCount", Validate.findAndCount, Middlewares.findAndCount, Services.findAndCount);

worker.use("findOne", Validate.findOne, Middlewares.findOne, Services.findOne);

export default worker;