//code generate automatic not edit date: 2023-11-24T01:42:29.686Z
import Joi from "onbbu/validate";
import * as T from "../types";

export const create = async (params: T.Create): Promise<T.Create> => {

  const schema = Joi.object({
    name: Joi.string().max(255).required(),
    lastName: Joi.string().max(255).required(),
    age: Joi.string().max(255).required(),
    status: Joi.boolean().required(),
    language: Joi.string().max(255).required(),
  });

  await schema.validateAsync(params);

  return params;
};

export const update = async (params: T.Update): Promise<T.Update> => {

  const schema = Joi.object({
    where: Joi.object({
      id: Joi.number().min(0).required(),
    }).required(),
    params: Joi.object({
      name: Joi.string().max(255),
      lastName: Joi.string().max(255),
      age: Joi.string().max(255),
      status: Joi.boolean(),
      language: Joi.string().max(255),
    }).required(),
  });

  await schema.validateAsync(params);

  return params;
};

export const destroy = async (params: T.Destroy): Promise<T.Destroy> => {

  const schema = Joi.object({
    where: Joi.object({
      id: Joi.array().items(Joi.number().min(0).required().required()).min(1).required(),
    }).required(),
  });

  await schema.validateAsync(params);

  return params;
};

export const findAndCount = async (params: T.FindAndCount): Promise<T.FindAndCount> => {

  const schema = Joi.object({
    paginate: Joi.object({
      limit: Joi.number().min(0),
      offset: Joi.number().min(0),
    }).required(),
    where: Joi.object({
    }).required(),
  });

  await schema.validateAsync(params);

  return params;
};

export const findOne = async (params: T.FindOne): Promise<T.FindOne> => {

  const schema = Joi.object({
    where: Joi.object({
      id: Joi.number().min(0).required(),
    }).required(),
  });

  await schema.validateAsync(params);

  return params;
};