//code generate automatic not edit date: 2023-11-24T01:13:29.483Z
//@onbbu-name: test
// export const name = "demo";
export const name = "fhjjcupwnwfhklh";
export interface ModelAttributes {
	id: number

	name: string
	lastName: string
	age: string
	status: boolean
	language: string

	createdAt: string
	updatedAt: string
}


export interface Create {
	name: string
	lastName: string
	age: string
	status: boolean
	language: string
}

export interface Update {
	where: {
		id: number
	}
	params: {
		name?: string
		lastName?: string
		age?: string
		status?: boolean
		language?: string
	}
}

export interface Destroy {
	where: {
		id: number[]
	}
}

export interface FindAndCount {
	paginate: {
		limit?: number
		offset?: number
	}
	where: {

	}
}

export interface FindOne {
	where: {
		id: number
	}
}



export const endpoint = [
  "create",
  "update",
  "destroy",
  "findAndCount",
  "findOne",
] as const;

export type Endpoint = typeof endpoint[number]